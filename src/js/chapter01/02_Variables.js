/*
* Declaration
*/

var num = 3;
console.log('num: ', num); // Output: 3

var trueValue = true;
console.log('trueValue: ', trueValue); // Output: true

var floatNum = 1.4;
console.log('floatNum: ', floatNum); // Output: 1.4

var str = 'My str here';
console.log('str: ', str); // Output: My str here

var und;
console.log('und: ', und); // Output: undefined

/*
* Variables scope
*/

var myVariable = 'global';
myOtherVariable = 'global';

function myFunction() {
  var myVariable = 'local';
  return myVariable;
}

function myOtherFunction() {
  myOtherVariable = 'local';
  return myOtherVariable;
}

console.log('myVariable: ', myVariable); // Output: global
console.log('myFunction: ', myFunction()); // Output: local

console.log('myOtherVariable', myOtherVariable); // Output: global
console.log('myOtherFunction', myOtherFunction()); // Output: local
console.log('myOtherVariable', myOtherVariable); // Output: local *Because `var` is omitted


